# Heart Rate Failure for WSL

A implementation of 
https://github.com/IBM/predictive-model-on-watson-ml/blob/master/notebooks/predictiveModel.ipynb 
for WSL

## Instructions

1. Create new Project
2. Add `patientdataV6.CSV` as dataset
3. Upload `predictiveModel.jupyter-py35.ipynb` notebook

After running all cells, a Spark model is created, saved, and tested with API call